<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Guzzle\Http\Client;

class CheckCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'check';

	/**
	 * Heartbeat config values.
	 */
	protected $hconfig;

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check sites in the `sites` table for their status.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->hconfig = Config::get('heartbeat');
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$servers = Server::all();

		foreach ($servers as $server) {

			$this->info("Checking server named '{$server->name}'...");

			foreach ($server->sites as $site) {

				$this->info("Checking site with URL '{$site->url}'");

				$client = new Client();

				$client->setUserAgent('Heartbeat HTTP Statusboard');

				$request = $client->get($site->url);

				try {
					$result = $client->send($request);
					$info = $result->getInfo();
					$http_code = $info['http_code'];
					$total_time = $info['total_time'];
				}
				catch (Guzzle\Http\Exception\CurlException $e) {
					$http_code = $e->getCode() == 0 ? 404 : $e->getCode();
					$methods = get_class_methods($e);
				}

				if ($http_code == 200) {

					$this->info("CHECK SUCCESSFUL");

				} else {

					$this->error("ERROR | http status: {$http_code}");

				}

				$this->info("HTTP CODE: {$http_code}");

				// Get the previous requests for this site.
				$lastRequest = CheckRequest::where('site_id', $site->id)->first();
                                $tweet = '';

				// Check if it was an errenous request.
				if ((empty($lastRequest) || $lastRequest->http_status == 200) && $http_code != 200) {
					// Error
					//$this->error("Attempting to send an email to notify {$this->hconfig['alert_email_to']}.");
                                        //$tweet = 'FORA DO AR: '.$site->name.'%0ASERVIDOR: '.$server->name.'%0AURL: '.$site->url.'%0AEM: '.date("d/m/Y : H:i:s");
                                        $tweet = 'FORA DO AR: '.$site->url.' ('.date("d/m/Y H:i:s").')';
					//Mail::send(
					//	'emails.site_down',
					//	array('site' => $site, 'date' => date("jS F Y : H:i:s")),
					//	function($message) {
					//		$message->to($this->hconfig['alert_email_to'])->subject('Site Down!');
					//	}
					//);

				} elseif (!empty($lastRequest) && $lastRequest->http_status != 200 && $http_code == 200) {
					// Ok
                                        //$tweet = 'NORMALIZADO: '.$site->name.'%0ASERVIDOR: '.$server->name.'%0AURL: '.$site->url.'%0AEM: '.date("d/m/Y : H:i:s");
                                        $tweet = 'NORMALIZADO: '.$site->url.' ('.date("d/m/Y H:i:s").')';
					//Mail::send(
					//	'emails.site_up',
					//	array('site' => $site, 'date' => date("jS F Y : H:i:s")),
					//	function($message) {
					//		$message->to($this->hconfig['alert_email_to'])->subject('Site Back Up!');
					//	}
					//);
				}

				if ($lastRequest instanceof CheckRequest) {
					$lastRequest->delete();
				}

				$request = new CheckRequest(array(
					'site_id' => $site->id,
					'http_status' => (int)$http_code,
					'response_time' => (float)$total_time,
				));

				$request->save();

                                if ($tweet != '') {
					//$twee = new Tweet(array(
					//	'twitter_timestamp'=> date("Y-m-d H:i:s"),
					//	'tweet_data'=>  $tweet,
					//));
					//$twee->save();
					Twitter::postTweet(['status' => $tweet, 'format' => 'json']); 
				}
				//$app_config = Config::get('heartbeat');
                                //$search_term=$app_config['twitter_search_term'];
				//$tweets = Twitter::getSearch(array('q' => $search_term ,'count' => 5));
				//$this->info("$search_term");

			}
			$this->comment('------------');
		}

	}

	/**
	 * Determine if now() falls within business hours.
	 * @return bool
	 */
	public function inBusinessHours()
	{
		$timeNow = time();

		// Create two boundary timestamps for today.
		$open = mktime(0, 0);
		$close = mktime(23, 59);

		// Is the current time between these two times?
		if ($timeNow > $open && $timeNow < $close) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
