<?php

// You can find the keys here : https://apps.twitter.com/

return [
	'debug'               => false,

	'API_URL'             => 'api.twitter.com',
	'UPLOAD_URL'          => 'upload.twitter.com',
	'API_VERSION'         => '1.1',
	'AUTHENTICATE_URL'    => 'https://api.twitter.com/oauth/authenticate',
	'AUTHORIZE_URL'       => 'https://api.twitter.com/oauth/authorize',
	'ACCESS_TOKEN_URL'    => 'https://api.twitter.com/oauth/access_token',
	'REQUEST_TOKEN_URL'   => 'https://api.twitter.com/oauth/request_token',
	'USE_SSL'             => true,

	'CONSUMER_KEY'        => function_exists('env') ? env('TWITTER_CONSUMER_KEY', 'hTMjYZpJ57UCvGLry5HeApi0P') : 'hTMjYZpJ57UCvGLry5HeApi0P',
	'CONSUMER_SECRET'     => function_exists('env') ? env('TWITTER_CONSUMER_SECRET', '5Mqo2kcPujUl9XBqtQuTGJrZopY8p6tJvbl45TDbnbNlZqbaJO') : '5Mqo2kcPujUl9XBqtQuTGJrZopY8p6tJvbl45TDbnbNlZqbaJO',
	'ACCESS_TOKEN'        => function_exists('env') ? env('TWITTER_ACCESS_TOKEN', '741006241464516609-8jMJv36oIhm3SOJpnAZDOwY9swwiTfo') : '741006241464516609-8jMJv36oIhm3SOJpnAZDOwY9swwiTfo',
	'ACCESS_TOKEN_SECRET' => function_exists('env') ? env('TWITTER_ACCESS_TOKEN_SECRET', 'yzOAlQlLeLUTxnuBXBtvnAw0Efj37ewyRIfmW0gc69aGr') : 'yzOAlQlLeLUTxnuBXBtvnAw0Efj37ewyRIfmW0gc69aGr',
];
